﻿using System;
using System.Collections.Generic;
using System.Linq;
using BinairePuzzleSOlver;
using BinairePuzzleSOlver.Solvers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BinairePuzzleSolver.Tests
{
    [TestClass]
    public class TwinNeightbourSolverTest
    {
        public IFieldRowSolver Solver { get; set; }
        public List<TestCase> Tests;

        [TestInitialize]
        public void Init()
        {
            Solver = new TwinNeightbourSolver();
            Tests = new List<TestCase>()
            {
                CreateTest(7, true, new List<bool?>(){null,null,null,null,null,false,false,null}),
                CreateTest(2, true, new List<bool?>(){false,false,null,null,null,null,null,null}),
                CreateTest(2, true, new List<bool?>(){false,false,null,null,null,true,false,false,true,true})/*,
                CreateTest(5, true, new List<bool?>(){null, null,null,null,null,null,false,false})/*,
                CreateTest(7, true, new List<bool?>())  */                              
            };
        }

        public TestCase CreateTest(int fieldNumber, bool? fieldValue, List<bool?> values)
        {
            return new TestCase(fieldNumber, fieldValue, new FieldRow() { Fields = values.Select(v => new FieldValue() { Value = v }).ToList() });
        }

        [TestMethod]
        public void Run()
        {
            foreach (var test in Tests)
            {
                Solver.Solve(test.FieldRow);
                Assert.AreEqual(test.FieldValue, test.FieldRow.Fields[test.FieldNumber].Value);
            }

        }

        public class TestCase : Tuple<int, bool?, FieldRow>
        {
            public TestCase(int item1, bool? item2, FieldRow item3) : base(item1, item2, item3)
            {
            }

            public int FieldNumber { get { return Item1; } }
            public bool? FieldValue { get { return Item2; } }
            public FieldRow FieldRow { get { return Item3; } }

        }
    }
}
