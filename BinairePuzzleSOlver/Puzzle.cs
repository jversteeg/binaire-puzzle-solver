﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver
{
    public class Puzzle
    {
        public int Size { get; set; }
        public FieldValue[,] Data { get; set; }
        public List<FieldRow>Rows { get; set; }

        public Puzzle(string content)
        {
            Data = PuzzleParser.ParseFields(content);
            Size = Convert.ToInt32(Math.Sqrt(Convert.ToDouble(Data.Length)));
            Rows = PuzzleParser.ParseRows(Data,Size).ToList();
        }

        public void Print()
        {
            for (var y = 0; y < Size; y++)
            {
                for (var x = 0; x < Size; x++)
                {
                    Console.Write("+---");
                }
                Console.WriteLine("+");
                for (var x = 0; x < Size; x++)
                {
                    var field = Data[x, y];
                    Console.Write("| ");
                    if (!field.Static)
                    {
                        Console.BackgroundColor = ConsoleColor.DarkGreen;
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    Console.Write(Data[x, y]);
                    if (!field.Static)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                    }
                    Console.Write(" ");
                }
                Console.WriteLine("|");
            }
            for (var x = 0; x < Size; x++)
            {
                Console.Write("+---");
            }
            Console.WriteLine("+");
        }

        public double SolvedPercentage()
        {
            double total = Size*Size;
            double solved = 0;
            for (var y = 0; y < Size; y++)
            {
                for (var x = 0; x < Size; x++)
                {
                    if (Data[x, y].Value.HasValue)
                    {
                        solved++;
                    }
                    else
                    {
                        
                    }
                }
            }
            return (100 /total)*solved;
        }
    }
}
