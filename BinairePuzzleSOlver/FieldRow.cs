﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver
{
    public class FieldRow
    {
        public FieldRow()
        {
            Fields = new List<FieldValue>();
        }
        public List<FieldValue> Fields { get; set; }
    }
}
