﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                var nr = Console.ReadKey();

                string text =
                    System.IO.File.ReadAllText(
                        @"c:\users\johan\documents\visual studio 2013\Projects\BinairePuzzleSOlver\BinairePuzzleSOlver\Puzzle\puzzle" +
                        nr.KeyChar + ".txt");
                var puzzle = new Puzzle(text);
                Console.Clear();
                puzzle.Print();
                Thread.Sleep(300);
                while (Solver.Solve(puzzle))
                {
                    Console.Clear();

                    ShowProgressbar(puzzle.SolvedPercentage(), (puzzle.Size*4)-2);

                    puzzle.Print();
                    //Console.ReadLine();
                }
                
                Console.WriteLine("Done");

                puzzle.SolvedPercentage();

                foreach (var solver in Solver.Solvers)
                {
                    Console.WriteLine(solver.GetType().Name + " " + solver.GetSolves());
                }

            }
        }

        static void ShowProgressbar(double progress, double size)
        {
            double slice = 100/size;
            Console.Write("[");
            
            for (var i = 0; i < size; i++)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.Write(slice * i <= progress ? "█" : " ");
                Console.ForegroundColor = ConsoleColor.White;
            }
            Console.Write("]");
            Console.Write(" = {0}%",progress);

            Console.WriteLine();
        }
    }
}
