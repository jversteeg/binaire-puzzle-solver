﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver
{
    public static class PuzzleParser
    {
        public static FieldValue[,] ParseFields(string content)
        {            
            var rows = content.Split('\n');            
            var size = rows.Count();
            var result = new FieldValue[size, size];
            for (var y = 0; y < size; y++)
            {
                for (var x = 0; x < size; x++)
                {
                    var value = rows[y][x];
                    bool? parsedValue = null;
                    if (value == '1')
                    {
                        parsedValue = true;
                    }
                    else if (value == '0')
                    {
                        parsedValue = false;
                    }
                    result[x, y] = new FieldValue() { Value = parsedValue, Static = parsedValue.HasValue };
                }
            }
            return result;
        }


        public static IEnumerable<FieldRow> ParseRows(FieldValue[,] content, int size)
        {
            // ->
            for (var y = 0; y < size; y++)
            {
                var fieldRow = new FieldRow();
                for (var x = 0; x < size; x++)
                {
                    fieldRow.Fields.Add(content[y,x]);
                }
                yield return fieldRow;
            }
            // <-
            for (var y = 0; y < size; y++)
            {
                var fieldRow = new FieldRow();
                for (var x = size-1; x >= 0; x--)
                {
                    fieldRow.Fields.Add(content[y, x]);
                }
                yield return fieldRow;
            }

            //    \/
            for (var x = 0; x < size; x++)
            {
                var fieldRow = new FieldRow();
                for (var y = 0; y < size; y++)
                {
                    fieldRow.Fields.Add(content[y, x]);
                }
                yield return fieldRow;
            }
            //    /\
            for (var x = 0; x < size; x++)
            {
                var fieldRow = new FieldRow();
                for (var y = size-1; y >= 0; y--)
                {
                    fieldRow.Fields.Add(content[y, x]);
                }
                yield return fieldRow;
            }
        }
    }
}
