﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver
{
    public class FieldValue
    {
        public bool? Value { get; set; }
        public bool Static { get; set; }

        public override string ToString()
        {
            switch (Value)
            {
                case true:
                    return "1";
                case false:
                    return "0";
                default:
                    return " ";
            }
        }
    }
}
