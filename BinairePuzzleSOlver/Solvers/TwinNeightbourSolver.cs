﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver.Solvers
{
    public class TwinNeightbourSolver : IFieldRowSolver
    {
        public static int Solves;

        public bool Solve(FieldRow row)
        {
            var size = row.Fields.Count;
            for (var i = 0; i < size; i++)
            {
                var fieldValue = row.Fields[i].Value;
                if (fieldValue != null)
                {
                    if (i < size - 2)
                    {
                        if (fieldValue == row.Fields[i + 1].Value)
                        {
                            if (i < size - 2)
                            {
                                if (row.Fields[i + 2].Value == null)
                                {
                                    row.Fields[i + 2].Value = !fieldValue;
                                    Solves++;
                                    return true;
                                }
                            }
                            if (i > 0)
                            {
                                if (row.Fields[i - 1].Value == null)
                                {
                                    row.Fields[i - 1].Value = !fieldValue;
                                    Solves++;
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
            return false;
        }

        public int GetSolves()
        {
            return Solves;
        }
    }
}
