﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver.Solvers
{
    public interface IFieldRowSolver
    {
        bool Solve(FieldRow row);
        int GetSolves();
    }
}
