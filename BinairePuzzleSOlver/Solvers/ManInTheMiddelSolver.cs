﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver.Solvers
{
    public class ManInTheMiddelSolver : IFieldRowSolver
    {
        public static int Solves;

        public bool Solve(FieldRow row)
        {
            var size = row.Fields.Count;
            for (var i = 0; i < size; i++)
            {
                var fieldValue = row.Fields[i].Value;
                if (fieldValue != null)
                {
                    if (i <= size - 3)
                    {
                        if (fieldValue == row.Fields[i + 2].Value)
                        {
                            if (row.Fields[i + 1].Value == null)
                            {
                                row.Fields[i + 1].Value = !fieldValue;
                                Solves++;
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public int GetSolves()
        {
            return Solves;
        }
    }
}
