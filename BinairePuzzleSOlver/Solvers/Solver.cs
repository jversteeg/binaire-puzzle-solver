﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinairePuzzleSOlver.Solvers;

namespace BinairePuzzleSOlver
{
    public class Solver
    {
        public static IEnumerable<IFieldRowSolver> Solvers
        {
            get
            {
                yield return new TwinNeightbourSolver();
                yield return new ManInTheMiddelSolver();
                yield return new RowOneMissingSolver();
            }
        }

        public static bool Solve(Puzzle puzzle)
        {
            foreach (var fieldRow in puzzle.Rows)
            {
                foreach (var rowSolver in Solvers)
                {
                    if (rowSolver.Solve(fieldRow))
                        return true;                    
                }
            }
            return false;
        }
    }
}
