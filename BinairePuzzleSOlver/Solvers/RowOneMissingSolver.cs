﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinairePuzzleSOlver.Solvers
{
    public class RowOneMissingSolver : IFieldRowSolver
    {
        public static int Solves;

        public bool Solve(FieldRow row)
        {
            var size = row.Fields.Count;
            var emptyFields = row.Fields.Where(f => f.Value == null).ToList();
            if (emptyFields.Any())
            {
                var trueFields = row.Fields.Where(f => f.Value == true).ToList();
                if (trueFields.Count() == size/2)
                {
                    emptyFields.ForEach(f=>f.Value=false);
                    Solves++;
                    return true;
                }
                var falseFields = row.Fields.Where(f => f.Value == false).ToList();
                if (falseFields.Count() == size / 2)
                {
                    emptyFields.ForEach(f => f.Value = true);
                    Solves++;
                    return true;
                }
            }
            return false;
        }


        public int GetSolves()
        {
            return Solves;
        }
    }
}
